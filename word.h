#ifndef WORD_H_
#define WORD_H_
#ifndef DEBUG
#define DEBUG 0
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FILE_NAME "ex_w.txt"

#define our_file "our_w.txt"

// turns current character into index from a - z
#define char_ind(b) ((int)b - (int)'a')
// this is the tree structure file

// Word structure with a score and the string it self
typedef struct words
{
	char w[100];
	int score;
}word;

//tree structure with array of 26 pointers to each letterin alphabet and things to indicate score and that it's at an end of a word
struct tree
{
	//point to all the alphabet (NULL if nothing is there)
	struct tree *children[26];

	// boolen expression integer. 0 if false 1 if true
	//if the current stream is a word then it's 1 if not it is 0
	int endofword;
	// score of the word which get inclemented by one if a word is used again 
	int score;
};

// Count lines in file
int countline(char *filename);

// Extract data to word pointer
word* extractdata(char *name);

// Print out word struct
void printmyword(word *x);

// Make sure our array is sorted
void sortarr(word* x);

// Make word dictionary
word* get_ex_w_dic();

// Create a word type with score one given a string
word make_word(char *x);

// Update size
void addonesize(word *before, int *count);

// If a word doesn't exist then add the word to the final array
word *add_new_word(word *final_arr, int *count, char *x);

// Given our final array and the # of items in that array, either find the same word
// add a score of 1 to its score, or if it's not found then add it to the final array and sort it
void add_score(word *final_arr, int *count, char *x);

int words();


// this is the tree structure file

// given nothing return a tree node (already malloced) the root/ nodes
struct tree *getnode(void);

// insert a non exisiting node. If the node exist then add score of 1 to it
void insert(struct tree *x, const char *y, int a);

int search_w_t(struct tree *x, const char *y);

// return 1 if all the children of the given node
// is NULL, if at lest one exist return 0
int endnode(struct tree *x);

// given a string and a character return a string with the exact same copy
// of the given string plus the character added at the end of it
char* add(char *x, char y);

// given a string
// return a string with the last character deleted from it
void sub(char *x);

// finds and prints all the subtree of the given tree(technecally a string)
void restofword(struct tree *x, char *y, word * result);

//print all the things in the sub tree (for now)
int printit(struct tree *x, const char *y, word *result);


#endif
