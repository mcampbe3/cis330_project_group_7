#ifndef PHRASE_H_
#define PHRASE_H_
#ifndef DEBUG
#define DEBUG 0
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Items in dictionary struct
typedef struct Dict_parts
{
	int score;
	char phrase[1000][1000];
    int len;
} Dict_part;

typedef struct trees
{
    char head[100];
    struct trees *next;
    int len;  // Number of elements in next
} p_tree;

// Get number of lines in file
int get_num_lines(FILE *file);

// Get number of characters in file
int get_char(FILE *file);

// Clean up file for phrase dictionary
void clean_up_file(const char *one, const char *two);

// Make a test dictionary
// Dict_part *make_test_dict();

// Sort dictionary by score
Dict_part **order_p_dict(Dict_part **dictionary, const char *file);

// Find the sentence in the tree
Dict_part *find_sentence(Dict_part *dict, char **input_array, int count, int len);

// Make user input test
char **make_test_input();

// Search for user input in tree
Dict_part *search(Dict_part *arr, char **input_word, int len, int num);

// Print words user would most likely use next
void print_predict(Dict_part *arr, int num);

// Search the tree
Dict_part *slow_tree_search(Dict_part *arr/*, char *input*/);

// Get the length of a file
int len_of_file(FILE *file);

// Get the number of lines in a file
int lines_in_file(FILE *file);

// Get the number of words in each line of a file
int *num_words_in_lines(FILE *file, int line_num);

// Initialize dictionary
Dict_part **init_dict(int line_num, int *lens);

// Put contents of file into dictionary
void load_phrases(FILE *file, Dict_part **dictionary, int length, int line_num, int *lens);

// Print dictionary
void print_dict(Dict_part **dictionary, int line_num, int *lens);

// Make dictionary
Dict_part **make_dic(const char *one);

// Make phrase tree
p_tree *make_phrase_tree(const char *one, Dict_part **arr);

int number_of_heads(const char *one, Dict_part **arr);

char **search_phrase_tree(p_tree *trees, char *given_word, int num_heads);

#endif
