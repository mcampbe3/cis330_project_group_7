#include <stdio.h>
#include <ncurses.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "word.h"
#include "phrase.h"

int key_pressed(void)
{
	// Initialize c
    int c = getch();
    // Check if a key is pressed
    if (c != ERR)
    {
        ungetch(c);
        return 1;
    }

    // If a key is not pressed 
    return 0;
}

int main()
{
  // Make a word array from external file
  word *final_arr = get_ex_w_dic();
  Dict_part **phrase_dict = make_dic("ex_p.txt");

  // Make phrase dictioanry and tree
  phrase_dict = order_p_dict(phrase_dict, "ex_p.txt");
  p_tree *phrase_tree = make_phrase_tree("ex_p.txt", phrase_dict);
  int num_heads = number_of_heads("ex_p.txt", phrase_dict);

  FILE *f;
  //open our.txt (our local word txt file)
  f = fopen(our_file, "r"); // read only
  // file not exisiting Error test case
  if(!f)
  {
    printf("Error! could not read file %s\n", our_file);
    exit(-1);
  }
  int lines = countline(our_file);
  int a = countline(FILE_NAME);
  int *counter = &a;
  char q[30];
  // basically add the score if there is a duplicate and a new thing in the array if the word doesn't exist
  for(int i = 0; i < lines; i++)
  {
    fscanf(f, "%s", q);
    add_score(final_arr, counter, q);
  }
  fclose(f);

  struct tree *root = getnode();

  int i;

  // creating our tree structure based on our array we just created ^
  for(i = 0; i < a -1; i++)
  {
    insert(root, final_arr[i].w, final_arr[i].score);
  }
	// Set up ncurses environment
    initscr();
    cbreak();
    noecho();
    nodelay(stdscr, TRUE);
    scrollok(stdscr, TRUE);

    // Initialize variables
    int count = 0;
    int long_count = 0;
    // this is our entire string
    char *word2 = malloc(1000);

    // this is our each individual strings
    char *curword = malloc(100);

    // this is the word dict which contains our two highest score finishing values of the current string
    word *options = (word *)malloc(sizeof(word) * 15);
    char **all_options = (char **)malloc(sizeof(char *) * 2);

    //this is the file that we will append the new strings into

    FILE *adder = fopen("our_w.txt", "a");
    FILE *pf = fopen("ex_p.txt", "a");

    int m;

    // Run the program
    while (1)
    {
    	// Check if key is pressed
        if (key_pressed())
        {
          char c = getch();

          // Don't add default instructions to print statement
          if (c != '1' && c != '2' && c != '3' && c != 127 && c != 8)
          {
            word2[long_count] = c;
            curword[count] = c;
          }

          // If user presses enter: exit program
          if (c == '\n')
          {
            // putting our string into our phrase text
            fprintf(pf, "%s", word2);

            // splitting each word by space and putting each individual word into our_w.txt

            char temp[100];
            int length = strlen(word2);
            int count = 0;
            for(int j = 0; j < length; j++)
            {
              if(word2[j] != ' ')
              {
                temp[count++] = word2[j];
              }
              else
              {
                fprintf(adder, "%s\n", temp);
                memset(temp, '\0', 100);
                count = 0;
              }
              if(j == length - 1)
              {
                fprintf(adder, "%s", temp);
              }
            }
            return 0;
          }

          // Delete one char from the word
          if (c == 127 || c == 8)
          {
            long_count--;
            count--;
            word2[long_count] = '\0';
            curword[count] = '\0';
            long_count--;
            count--;
            printw("\r");
          }

          // User choose first choice
          if (c == '1')
          {
            long_count = long_count - count;
            for (int j = long_count; j < long_count + count; j++)
            {
              word2[j] = '\0';
            }
            strcat(word2, all_options[0]);
            long_count += strlen(all_options[0]) - 1;
            count = -1;
            printw("\r");
            memset(curword, '\0', 100);
          }

          // User choose second choice
          if (c == '2')
          {
            long_count = long_count - count;
            for (int j = long_count; j < long_count + count; j++)
            {
              word2[j] = '\0';
            }
            strcat(word2, all_options[1]);
            long_count += strlen(all_options[1]) - 1;
            count = -1;
            printw("\r");
            memset(curword, '\0', 100);
          }

          // If user types a space: predict next word
          // Otherwise: predict rest of word
          if (c == ' ')
          {
            word2[long_count] = c;
            memset(all_options, '\0', sizeof(char *) * 2);
            char **p_options = (char **)malloc(sizeof(char *) * 2);
            p_options = search_phrase_tree(phrase_tree, word2, num_heads);
            if (p_options[0] != NULL)
            {
              all_options[0] = p_options[0];
            }
            else
            {
              all_options[0] = NULL;
            }
            if (p_options[1] != NULL)
            {
              all_options[1] = p_options[1];
            }
            else
            {
              all_options[1] = NULL;
            }
            memset(curword, '\0', count);
            count = -1;
          }
          else
          {
            memset(options, '\0', sizeof(word) * 15);
            memset(all_options, '\0', sizeof(char *) * 2);
            m = printit(root, curword, options);
            if (strlen(options[0].w) > 0)
            {
              all_options[0] = options[0].w;
            }
            else
            {
              all_options[0] = NULL;
            }
            if (strlen(options[1].w) > 0)
            {
              all_options[1] = options[1].w;
            }
            else
            {
              all_options[1] = NULL;
            }
          }

          // After each character a user enters
          printw("%s", word2);
          refresh();
          count++;
          long_count++;

          // If the user has given input: print options
          if (strlen(word2) > 0)
          {
            if (all_options[1] != NULL)
            {
              printw("\nOptions ->\t1 : %s \t2 : %s\n\n", all_options[0], all_options[1]);
            }
            else if (all_options[0] != NULL)
            {
              printw("\nOptions ->\t1 : %s\n\n", all_options[0]);
            }
            else
            {
              printw("\nNo options found\n\n", all_options[0], all_options[1]);
            }
          }
        }
        else
        {
            refresh();
            sleep(1);
        }
    }
}
