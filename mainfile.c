#include<stdio.h>
#include <termios.h>    // termios, TCSANOW, ECHO, ICANON
#include <unistd.h>     // STDIN_FILENO
#include <stdlib.h>
#include <ncurses.h> // adden ncurses to try to accomodate printw();


//External Files
#include "word.h"
#include "phrase.h"


// Small text graphics to explain to user on how to work the program
int intro()
{
    printf("Welcome to our Predictive Text Program\n");
    printf("Begin typing and if you see the word you \n");
    printf("are looking for press  |  1  |  2  |  3  | respectively.\n");
    printf("Hitting 'Enter' will exit the program. \n");
    printf("\n");
    printf("\n");

    return 0;
}

int main(void)
{   
    char c;   
    static struct termios oldt, newt;

    // tcgetattr gets the parameters of the current terminal
    // STDIN_FILENO will tell tcgetattr that it should write the settings
    // of stdin to oldt
    tcgetattr( STDIN_FILENO, &oldt);

    // now the settings will be copied
    newt = oldt;

    // ICANON normally takes care that one line at a time will be processed
    // that means it will return if it sees a "\n"
    newt.c_lflag &= ~(ICANON | ECHOE);  

    // Those new settings will be set to STDIN
    // TCSANOW tells tcsetattr to change attributes immediately
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);

    // calling intro function 
    intro();


    // initialize word array qith corresponding dictionaries loaded
    word *wordArr = get_ex_w_dic();
    Dict_part **phrase_dict = make_dic("ex_p.txt");
    p_tree *phrase_tree = make_phrase_tree("ex_p.txt", phrase_dict);
    int heads = number_of_heads("ex_p.txt", phrase_dict);

    FILE *filename;
    filename = fopen(our_file, "r"); // open file

    int lines = countline(our_file);
    int linesInFN = countline(FILE_NAME);
    int *cntr = &linesInFN;
    char wordSize[30];

    // add scores to all words
    for(int i = 0; i < lines; i++)
    {
        fscanf(filename, "%s", wordSize);
        add_score(wordArr, cntr, wordSize);
    }

    fclose(filename);   // close file

    // initialize the tree structure
    struct tree *root = getnode();

    // inserting elements into the tree
    for(int j = 0; j < linesInFN -1; j++)
    {
        insert(root, wordArr[j].w, wordArr[j].score);
    }

    int wordCnt = 0;
    int userCnt = 0;

    // all strings the user types
    char *userStr = malloc(1000);

    // each word string
    char *wordStr = malloc(100);

    // word dictionary + all optiona available 2d array
    word *options = (word *)malloc(sizeof(word) * 15);
    char **totalOptions = (char **)malloc(sizeof(char *) * 3);

    // external files that will have items appended to them
    FILE *ourWTxt = fopen("our_w.txt", "a");
    FILE *exPTxt = fopen("ex_p.txt", "a");

    int printIt;

    printf("> ");

    /**************************** Loop Structure *****************************************/
    while(( c = getchar()) != '\n')
    {
        // get character each time user hits a letter
        if(c == '1')
        {
            // chosen word in 1 spot
            userCnt = userCnt - wordCnt;
            for (int j = userCnt; j < userCnt + wordCnt; j++)
            {
              userStr[j] = '\0';
            }
            strcat(userStr, totalOptions[0]);
            userCnt += strlen(totalOptions[0]) - 1;
            wordCnt = -1;
            //delete all the way back to beginnning of word for replacement word to go 
            for(int y = 0; y < wordCnt; y ++)
            {
                printf("\b");
                printf(" ");
                printf("\b");
            }
            memset(wordStr, '\0', 100);
            printf("> ");

        }
        if(c == '2')
        {
            // chosen word in 2 spot
            printf("\n");
            userCnt = userCnt - wordCnt;
            for (int l = userCnt; l < userCnt + wordCnt; l++)
            {
              userStr[l] = '\0';
            }
            strcat(userStr, totalOptions[1]);
            userCnt += strlen(totalOptions[1]) - 1;
            wordCnt = -1;
            //delete all the way back to beginnning of word for replacement word to go 
            for(int x = 0; x < wordCnt; x ++)
            {
                printf("\b");
                printf(" ");
                printf("\b");
            }
            memset(wordStr, '\0', 100);
            printf("> ");
        }
        if(c == '3')
        {
            //chosen word in 3 spot
            printf("\n");
            userCnt = userCnt - wordCnt;
            for (int k = userCnt; k < userCnt + wordCnt; k++)
            {
              userStr[k] = '\0';
            }
            strcat(userStr, totalOptions[2]);
            userCnt += strlen(totalOptions[2]) - 1;
            wordCnt = -1;
            //delete all the way back to beginnning of word for replacement word to go 
            for(int y = 0; y < wordCnt; y ++)
            {
                printf("\b");
                printf(" ");
                printf("\b");
            }
            memset(wordStr, '\0', 100);
            printf("> ");
        }
        if(c == ' ')
        {
            // when user types space it will give prediction if possible
            userStr[userCnt] = c;
            memset(totalOptions, '\0', sizeof(char *) * 3);
            char **possibleOptions = (char **)malloc(sizeof(char *) * 3);
            possibleOptions = search_phrase_tree(phrase_tree, userStr, heads);
            if (possibleOptions[0] != NULL)
            {
              totalOptions[0] = possibleOptions[0];
            }
            else
            {
              totalOptions[0] = NULL;
            }
            if (possibleOptions[1] != NULL)
            {
              totalOptions[1] = possibleOptions[1];
            }
            else
            {
              totalOptions[1] = NULL;
            }
            if (possibleOptions[2] != NULL)
            {
              totalOptions[2] = possibleOptions[2];
            }
            else
            {
              totalOptions[2] = NULL;
            }
            memset(wordStr, '\0', wordCnt);
            wordCnt = -1;
        }
        if(c == 127 || c == 8)
        {
            // this allows us to simulate 'backspace' in the program
            // if user bacskpace add a space after to componsate deletion.
            userCnt--;
            wordCnt--;
            userStr[userCnt] = '\0';
            wordStr[wordCnt] = '\0';
            userCnt--;
            wordCnt--;
            printf("\b");
            printf(" ");
            printf("\b");
        }
        else
        {
            // this will display what we want in the terminal one character at a time
            //putchar(c);
            memset(options, '\0', sizeof(word) * 15);
            memset(totalOptions, '\0', sizeof(char *) * 3);
            printIt = printit(root, wordStr, options);
            if (strlen(options[0].w) > 0)
            {
              totalOptions[0] = options[0].w;
            }
            else
            {
              totalOptions[0] = NULL;
            }
            if (strlen(options[1].w) > 0)
            {
              totalOptions[1] = options[1].w;
            }
            else
            {
              totalOptions[1] = NULL;
            }
            if (strlen(options[2].w) > 0)
            {
              totalOptions[2] = options[2].w;
            }
            else
            {
              totalOptions[2] = NULL;
            }
        }
        // increment counter to continue down strings
        printw("%s", userStr);
        wordCnt++;
        userCnt++;

        if (strlen(userStr) > 0)
        {
            if (totalOptions[1] != NULL)
            {
              printw("\n |\t1 -> %s | \t2 -> %s | \t3 -> %s |\n\n", totalOptions[0], totalOptions[1], totalOptions[2]);
            }
            else
            {
              printw("\nNo predictions currently...Keep typing\n\n", totalOptions[0], totalOptions[1], totalOptions[2]);
            }
        }

    }
    // displays this when user hits the enter key
    printf("\n");
    printf("\n");
    printf("End of Predictive Text...\n");
    printf("Thanks For Playing...\n");
    printf("Exiting...\n");
    printf("\n"); 


    // restore the old terminal settings
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt);

    return 0;
}