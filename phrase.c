#include "phrase.h"

/* START OF PHRASE TEXT FILE CLEANUP FUNCTIONS */

// Get number of lines in file
int get_num_lines(FILE *file)
{
	int num_lines = 0;
	char c = getc(file);
	while((c=getc(file)) != EOF)
	{
		if (c == '\n')
		{
			num_lines++;
		}
	}
	fseek(file, 0, SEEK_SET);
	return num_lines;
}

// Get number of characters in file
int get_char(FILE *file)
{
	fseek(file, 0, SEEK_END);
	int num_chars = ftell(file);
	fseek(file, 0, SEEK_SET);
	return num_chars;
}

// Clean up file for phrase dictionary
void clean_up_file(const char *one, const char *two)
{
	// Initialize variables
	// char c;
	int match = 0;
	int appended;
	char *alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz -',";

	// Open first file to read
	FILE *f;
	f = fopen(one, "r");

	// Get number of lines in file
	int lines = get_num_lines(f);

	// Get number of characters in file
	int char_in_file = get_char(f);

	// Open second file to write
	FILE *f2;
	f2 = fopen(two, "w");

	// Clean up the file
	for (int i = 0; i < lines; i++)
	{
		// Initialize variables
		char *line = NULL;
		appended = 0;
		int cut_off = 0;

		// Get each line in the file
		fgets(line, char_in_file, f);

		// Get and clean each line in file
		if (i > 514 && i < 33000 && strlen(line) > 3 && line[1] != '\n' && line[0] != ' ')
		{
			for (unsigned long j = 0; j < strlen(line); j++)
			{
				cut_off = 0;
				match = 0;
				for (unsigned long k = 0; k < strlen(alpha); k++)
				{
					if (line[j] == alpha[k])
					{
						match = 1;
						break;
					}
					else if (k == strlen(alpha) - 1 && match == 0)
					{
						char *tmp = (char *)malloc(sizeof(char) * j);
						strncpy(tmp, line, j);
						memset(line, 0, strlen(line));
						strcpy(line, tmp);
						cut_off = 1;
					}
				}
				if (cut_off)
				{
					break;
				}
			}

			// Write cleaned up lines to second file
			if (strcmp(line, "SECTION") != 0)
			{
				fprintf(f2, "%s\n", line);
			}
		}
	}

	// Close files
	fclose(f);
	fclose(f2);
}

/* START OF PHRASE DICTIONARY FUNCTIONS */

// Get the length of a file
int len_of_file(FILE *file)
{
	fseek(file, 0, SEEK_END);
	return ftell(file);
}

// Get the number of lines in a file
int lines_in_file(FILE *file)
{
	int lines = 0;
	char c;
	while((c=getc(file)) != EOF)
	{
		if (c == '\n')
		{
			lines++;
		}
	}
	return lines;
}

// Get the number of words in each line of a file
int *num_words_in_lines(FILE *file, int line_num)
{
	int counter = 0;
	int spot_count = 0;
	char c;
	int *lens = (int *)malloc(sizeof(int) * line_num);
	while((c=getc(file)) != EOF)
	{
		if (c == ' ')
		{
			counter++;
		}
		if (c == '\n')
		{
			counter++;
			lens[spot_count] = counter;
			spot_count++;
			counter = 0;
		}
	}
	return lens;
}

Dict_part **order_p_dict(Dict_part **dictionary, const char *file)
{
	FILE *f;
	f = fopen(file, "r");
	int lines = lines_in_file(f);
	for (int i = 0; i < lines - 1; i++)
	{        
       for (int j = 0; j < lines - i - 1; j++)
       { 			
           if (dictionary[j]->score < dictionary[j + 1]->score) 
           {
            	Dict_part tmp = *dictionary[j]; 
			    *dictionary[j] = *dictionary[j + 1]; 
			    *dictionary[j + 1] = tmp;
           }
        }
    }

    // See if dictionary is sorted
	// for (int i = 0; i < 5; i++)
	// {
	// 	for (int j = 0; j < 3; j++)
	// 	{
	// 		printf("%s ", dictionary[i]->phrase[j]);
	// 	}
	// 	printf("%d\n\n", dictionary[i]->score);
	// }

    // Return
    return dictionary;
}

// Initialize dictionary
Dict_part **init_dict(int line_num, int *lens)
{
	Dict_part **dictionary;
	dictionary = (Dict_part **)malloc(sizeof(Dict_part *) * line_num);
	for (int i = 0; i < line_num; i++)
	{
		dictionary[i] = (Dict_part *)malloc(sizeof(Dict_part) * lens[i]);
	}
	return dictionary;
}

// Put contents of file into dictionary
void load_phrases(FILE *file, Dict_part **dictionary, int length, int line_num, int *lens)
{
	// Initialize variables
	char *word;
	// char *line = NULL;
	// char word_copy[length];
	char buffer[length];

	// Loop through all the lines in the file
	for (int i = 0; i < line_num; i++)
	{
		Dict_part *part;
		part = (Dict_part *)malloc(sizeof(Dict_part));
		int count = 0;
		char ph[lens[i]][length];
		fgets(buffer, length, file);
		word = strtok(buffer, " ");
		strcpy(ph[count], word);
		count++;

		// Loop through each word in the line
		while(word != NULL)
		{
			word = strtok(NULL, " ");
			if (word != NULL)
			{
				strcpy(ph[count], word);
			}
			count++;
		}

		// Loop through words in line to copy them to Dict_part
		for (int j = 0; j < lens[i]; j++)
		{
			strcpy(part->phrase[j], ph[j]);
		}
		// Check for duplicates in dictionary
		for (int k = 0; k < i; k++)
		{
			int copy = 0;
			if (dictionary[k]->len == count)
			{
				for (int l = 0; l < count; l++)
				{
					if (strcmp(dictionary[k]->phrase[l], part->phrase[l]) == 0)
					{
						copy++;
					}
					else
					{
						break;
					}
				}
				if (copy == count)
				{
					dictionary[k]->score++;
				}
			}
		}

		// Loop through words in line to copy them to dictionary

		for (int k = 0; k < lens[i]; k++)
		{
			if (!dictionary[i]->score)
			{
				strcpy(dictionary[i]->phrase[k], part->phrase[k]);
				//dictionary[i]->score = 1;
				dictionary[i]->len = count;
			}
		}

	}
}

// Print dictionary
void print_dict(Dict_part **dictionary, int line_num, int *lens)
{
	for (int i = 0; i < line_num; i++)
	{
		for (int j = 0; j < lens[i]; j++)
		{
			printf("%s ", dictionary[i]->phrase[j]);
		}
		printf("%d\n\n", dictionary[i]->score);
	}
}

// Make dictionary
Dict_part **make_dic(const char *one)
{
	// Open and read the file
	FILE *f;
	f = fopen(one, "r");

	// Get the length of the file
	int len = len_of_file(f);
	fseek(f, 0, SEEK_SET);  // Go back to the start of the file

	// Get the number of lines in the file
	int lines = lines_in_file(f);
	fseek(f, 0, SEEK_SET);  // Go back to the start of the file

	// Get number of words in each line of the file
	int *line_len = num_words_in_lines(f, lines);
	fseek(f, 0, SEEK_SET);  // Go back to the start of the file

	// Initiliaze the dictionary
	Dict_part **phrase_dict = init_dict(lines, line_len);

	// Load the contents of the file into a dictionary
	load_phrases(f, phrase_dict, len, lines, line_len);

	// Print phrase dictionary
	//print_dict(phrase_dict, lines, line_len);

	// Close the file
	fclose(f);
	return phrase_dict;
}

/* START OF PHRASE TREE FUNCTIONS */

char **make_test_input(int num)
{
	char **input_word = (char **)malloc(sizeof(char *) * num);
	input_word[0] = "maybe";
	input_word[1] = "it";
	return input_word;
}

void print_predict(Dict_part *arr, int num)
{
	for (int j = 0; j < 2; j++)
	{
		printf("%s %d\n", arr[j].phrase[num], arr[j].score);
	}
}

void print_heads(char **heads, int len)
{
	for (int j = 0; j < len; j++)
	{
		printf("HEAD%d: %s\n", j, heads[j]);
	}
}

p_tree *make_phrase_tree(const char *one, Dict_part **arr)
{
	// Test input word
	// char **input_word = (char **)malloc(sizeof(char *) * num);
	// input_word[0] = "maybe";
	// input_word[1] = "it";

	// Open text file to read
	FILE *f;
	f = fopen(one, "r");

	// Get number of lines in the file
	int len = get_num_lines(f);

	// Get number of words in each line
	int *lens = num_words_in_lines(f, len);

	// Create an array of unique heads
	char *heads[len];
	char *prev = arr[0]->phrase[0];
	heads[0] = prev;
	int num_heads = 1;

	// Create array of trees
	p_tree *trees;
	trees = (p_tree *)malloc(sizeof(p_tree) * len);
	strcpy(trees[0].head, arr[0]->phrase[0]);

	// Get the unique heads in file
	for (int i = 1; i < len; i++)
	{
		if (strcmp(arr[i]->phrase[0], prev) != 0)
		{
			heads[num_heads] = arr[i]->phrase[0];
			strcpy(trees[num_heads].head, arr[i]->phrase[0]);
			trees[num_heads].next = (p_tree *)malloc(sizeof(p_tree) * 100);
			trees[num_heads].len = 0;
			num_heads++;
			prev = arr[i]->phrase[0];
		}
	}

	// Print out heads
	//print_heads(heads, num_heads);

	for (int i = 0; i < len; i++)
	{
		for (int j = 0; j < num_heads; j++)
		{
			if (strcmp(arr[i]->phrase[0], heads[j]) == 0)
			{
				p_tree *tmp = &trees[j];
				if (tmp->len == 0)
				{
					tmp->next = malloc(sizeof(p_tree) * len);
				}
				for (int k = 1; k < lens[i]; k++)
				{
					int place;
					int found = 0;
					for (int l = 0; l < tmp->len; l++)
					{
						if (strcmp(tmp->next[l].head, arr[i]->phrase[k]) == 0)
						{
							found = 1;
							place = l;
						}
					}
					if (found)
					{
						tmp = &tmp->next[place];
					}
					else
					{
						strcpy(tmp->next[tmp->len].head, arr[i]->phrase[k]);
						tmp->len++;
						tmp = &tmp->next[tmp->len - 1];
						tmp->next = malloc(sizeof(p_tree) * 100);
					}
				}
			}
		}
	}

	// Check tree
	// for (int j = 0; j < num_heads; j++)
	// {
	// 	printf("\nHEAD%d: %s %d\n", j, trees[j].head, trees[j].len);
	// 	printf("\nNEXT1 HEAD%d: %s %d\n", j, trees[j].next[0].head, trees[j].next[0].len);
	// 	if (j == 2)
	// 	{
	// 		printf("\nNEXT2 HEAD%d: %s %d\n", j, trees[j].next[1].head, trees[j].next[1].len);
	// 	}
	//  	printf("\nNEXT NEXT HEAD%d: %s %d\n", j, trees[j].next[0].next[0].head, trees[j].next[0].next[0].len);
	// }
	
	return trees;
}

int number_of_heads(const char *one, Dict_part **arr)
{
	FILE *f;
	f = fopen(one, "r");

	// Get number of lines in the file
	int len = get_num_lines(f);

	// Create an array of unique heads
	char *heads[len];
	char *prev = arr[0]->phrase[0];
	heads[0] = prev;
	int num_heads = 1;

	// Create array of trees
	p_tree *trees;
	trees = (p_tree *)malloc(sizeof(p_tree) * len);
	strcpy(trees[0].head, arr[0]->phrase[0]);

	// Get the unique heads in file
	for (int i = 1; i < len; i++)
	{
		if (strcmp(arr[i]->phrase[0], prev) != 0)
		{
			heads[num_heads] = arr[i]->phrase[0];
			strcpy(trees[num_heads].head, arr[i]->phrase[0]);
			trees[num_heads].next = (p_tree *)malloc(sizeof(p_tree) * 100);
			trees[num_heads].len = 0;
			num_heads++;
			prev = arr[i]->phrase[0];
		}
	}

	return num_heads;
}

char **search_phrase_tree(p_tree *trees, char *given_word, int num_heads)
{
	// Loop through all the lines in the file
	
	int loop_count = 0;
	char *word;
	char *g_word = (char *)malloc(sizeof(char) * 100);
	strcpy(g_word, given_word);
	word = strtok(g_word, " ");
	p_tree *tmp = trees;
	int not_found = 1;

	while(word != NULL)
	{
		int J = tmp->len;

		if (loop_count == 0)
		{
			J = num_heads;
		}
		for (int j = 0; j < tmp->len; j++)
		{
			char *cmp = tmp->next[j].head;
			if (loop_count == 0)
			{
				cmp = tmp[j].head;
			}
			if (strcmp(cmp, word) == 0)
			{
				if (loop_count == 0)
				{
					*tmp = tmp[j];
				}
				else if (j != tmp->len - 1)
				{
					*tmp = tmp->next[j];
				}
				break;
			}
			else if (j == tmp->len - 1)
			{
				not_found = 0;
			}
		}
		word = strtok(NULL, " ");
		loop_count++;
	}

	// Free
	free(g_word);

	char **ret = (char **)malloc(sizeof(char *) * 2);
	if (not_found == 1)
	{
		if (tmp->len > 0)
		{
			ret[0] = tmp->next[0].head;
		}
		if (tmp->len > 1)
		{
			ret[1] = tmp->next[1].head;
		}
	}
	else
	{
		ret[0] = NULL;
		ret[1] = NULL;
	}
	return ret;
}
