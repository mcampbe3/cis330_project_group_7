CIS 330 Final Project

List of Files
-------------

ex_w.txt		External word text document
ex_p.txt		External phrases text document
our_w.txt		Internal word text document(gets read and written into)
our_p_dict_test.txt	Internal phrases text document

word.c			Word data structure implementation(reads from ex_w.txt and our_w.txt) it then creates a tree strucutre based on the score in the word data structure
word.h			Header file for word.c
phrases.c		Phrases data sturcture including the data structure and the tree
phrases.h		Heaader file for phrases.c
pretxt.c		Our main file
Makefile		our make file that puts everything together


Running the project
-------------

The program is ran by typing
someonesname$ make
someonesname$ ./pretxt
then run make clear to clear out the files made

After you type that please give it a few seconds (as the data structrues and trees are getting created)
After around 10-20 seconds have passed it should put the user into a blank terminal
Now, type a word (one character at a time) and either finish writing the word you were thinking of
Or if you see your word in one of the choices press that buttom (1 or 2)
After so your word should appear in the writing part. If this is the case press enter
and continue to type other words as you please. To exit press enter
(Note that buttoms 1 and 2 are to not be used other than to choose an option)

-
Cody's Implementation

mainfile.c        main function with loop and data structures
p_main.c          skeleton of the main function on how it should run 

To run open the Makefile and change 'pretext.c' to 'mainfile.c'
Same with them wait until you see a ">" appear which will indicate it is ready for input processing...

...However it probably will only allow you to type one letter.
I was hit with a long series of "Segmentation fault: 11" over the course of the past few days, to no avail of my terminal. 
What I have gathered is that it creates all the trees, dictonaries, and all necessary data structures in order to run.
Yet somewhere I am trying to access memory that isn't available. 
Maybe I am trying to do something that their code doesnt allow me to do, and that is why I have the Seg Fault, I tried to read up and look but didnt see any place where I would be interfering with their code.
I know it's not a running and fully functioning terminal and im sorry for that, I  truly tried my best to get it done. I will take whatever grade you deem fit for the code I have written, working or not. Thank you for giving me at least a chance to redeem myself.
