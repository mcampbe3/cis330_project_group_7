# Makefile for word, phrase, and pretxt

# Make file to run
target = pretxt
sources = $(wildcard *.c)
objects = $(addsuffix .o, $(basename $(sources)))
flags = -g -W -Wall -std=c11 -DDEBUG=1 -DTEST=0 -I./usr/include -lcurses

# Make target
$(target): phrase.o pretxt.o word.o
	gcc -o $(target) phrase.o pretxt.o word.o $(flags)

# Make phrase object file
phrase.o: phrase.c phrase.h
	gcc -c $(flags) $< -o $@

# Make pretxt object file
pretxt.o: pretxt.c
	gcc -c $(flags) $< -o $@

# Make word object file
word.o: word.c word.h
	gcc -c $(flags) $< -o $@

# Clean up what we made
clean:
	rm $(target) phrase.o pretxt.o word.o
