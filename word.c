#include "word.h"

/* START OF WORD DICTIONARY FUNCTIONS*/

// this function counts how many line(items) are in a file
int countline(char *filename)
{
	FILE *fp;
	fp = fopen(filename, "r");

	int ch;
	ch = 0;

	int lines;
	lines = 0;

	if(fp == NULL)
	{
		return 0;
	}
	//there's at least one line
	lines++;
	while((ch = fgetc(fp)) != EOF)
	{
		if(ch == '\n')
		{
			lines++;
		}
	}

	fclose(fp);
	return lines;
}

// this function reads all the elements in the given file name and returns a array of words with a score of 1
// (assuming there are no repeated words)
word* extractdata(char *name)
{
	FILE *filename;
	filename = fopen(name, "r"); // read only

	// file not exisiting Error test case
	if(! filename)
	{
		printf("Error! could not read file %s\n", FILE_NAME);
		exit(-1);
	}
	int lines;
	lines = countline(FILE_NAME);

	char q[100];

	word *val;
	val = malloc(sizeof(word) * lines);



	for(int i = 0; i < lines; i++)
	{
		word temp;

		fscanf(filename, "%s", q);
		strcpy(temp.w, q);
		temp.score = 1;
		val[i] = temp;
	}
	fclose(filename);
	return val;
}

// this function although not used was used to check and test the above codes
void printmyword(word *x)
{
	int q;
	q = countline(FILE_NAME);

	for(int i = 0; i < q; i++)
	{
		printf("string is %s\n", x[i].w);
		printf("score is %d\n\n", x[i].score);
	}
	return;
}


//make sure our array is sorted
// although it doesn't help much it will compute things a little quicker for
// words that start eariler in the alphabet
void sortarr(word* x)
{
	int n;
	char temp[100];
	n = countline(FILE_NAME);

	for(int i = 0; i < n - 1; i++)
	{
		for(int j = i + 1; j < n; j++)
		{
			if(strcmp(x[i].w, x[j].w) > 0)
			{
				strcpy(temp, x[i].w);
				strcpy(x[i].w, x[j].w);
				strcpy(x[j].w, temp);
			}
		}
	}
}

// this is at actual function to use for extracting the data
// because it's stored in a dynamic memory
word* get_ex_w_dic()
{
	word *final_arr;
	int p;
	p = countline(FILE_NAME);
	final_arr = malloc(sizeof(word) * p);

	final_arr = extractdata(FILE_NAME);
	sortarr(final_arr);

	//printmyword(final_arr);

	return final_arr;
}

/* this file creates our users word of dictionaries which consistes of
the external dictionary of list of words with score of 1 and possible
words that the user uses that is not in the external dictionary will be added to this dictionary
*/

//create a word type with score one given a string
word make_word(char *x)
{
	word temp;
	// temp.score = malloc(sizeof(int));
	// temp.w = malloc(sizeof(char) * strlen(x));
	temp.score = 1;
	strcpy(temp.w, x);
	return temp;
}

// although this function wasn't used at the very end (because I found a way to run things a little faster)
// it was a good idea that later helped me to get to where I am at
void addonesize(word *before, int *count)
{
	word *temp;
	temp = malloc(sizeof(word) * (*count + 1));
	for(int i = 0; i < *count; i++)
	{
		strcpy(temp[i].w, before[i].w);
		temp[i].score = before[i].score;
	}
	sortarr(temp);
	before = temp;
	free(temp);
}

// if a word doesn't exist then add the word to the final array
word *add_new_word(word *final_arr, int *count, char *x)
{
	word temp = make_word(x);
	addonesize(final_arr, count);
	count++;
	final_arr[*count] = temp;
	return final_arr;
}


// given our final array and the # of items in that array, either find the same word
// add a score of 1 to its score, or if it's not found then add it to the final array and sort it
void add_score(word *final_arr, int *count, char *x)
{
	//for all the elements in the array
	for(int i = 0; i < *count; i++)
	{
		// if found
		if(strcmp(final_arr[i].w, x) == 0)
		{
			// add one to its score
			final_arr[i].score += 1;
			return;
		}
	}
	// if not found we get here
	final_arr = add_new_word(final_arr, count, x);
	//printf("ooh nooooooooo\n\n\n");
	return;

}
/* START OF WORD TREE FUCNTIONS */

// given nothing return a tree node (already malloced) the root/ nodes
struct tree *getnode(void)
{
	struct tree *curnode = NULL;

	curnode = (struct tree *)malloc(sizeof(struct tree));

	int i;

	curnode->endofword = 0;

	for(i = 0; i < 26; i++)
	{
		curnode->children[i] = NULL;
	}

	return curnode;
}

// insert a non exisiting node. If the node exist then add score of 1 to it
void insert(struct tree *x, const char *y, int a)
{
	int i;
	int ind;
	int length = strlen(y);

	struct tree *p = x;

	for(i = 0; i < length; i++)
	{
		ind = char_ind(y[i]);
		if(!p->children[ind])
		{
			p->children[ind] = getnode();
		}
		p = p->children[ind];
	}
	if(p->endofword)
	{
		p->score += 1;
		return;
	}
	p->endofword = 1;
	p->score = a;
	return;
}

// search the tree for the given string y if the string exists then return 1 otherwise return 0
// although I don't think i used this function it was a good testing function
int search_w_t(struct tree *x, const char *y)
{
	int i;
	int ind;
	int length = strlen(y);
	struct tree *p = x;

	for(i = 0; i < length; i++)
	{
		ind = char_ind(y[i]);

		if(!p->children[ind])
		{
			return 0;
		}
		p = p->children[ind];
	}

	if(p != NULL && p->endofword)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


// return 1 if all the children of the given node
// is NULL, if at lest one exist return 0
int endnode(struct tree *x)
{
	int i;
	for(i = 0; i < 26; i++)
	{
		if(x->children[i])
		{
			return 0;
		}
	}
	return 1;
}
// given a string and a character return a string with the exact same copy
// of the given string plus the character added at the end of it
char* add(char *x, char y)
{
	int i;
	int length = strlen(x);
	char *r = malloc(sizeof(char) * (length + 1));
	for(i = 0; i < length; i++)
	{
		r[i] = x[i];
	}
	r[length] = y;
	r[length + 1] = '\0';
	return r;
}

// given a string
// return a string with the last character deleted from it
void sub(char *x)
{
	int length = strlen(x);
	x[length-1] = '\0';
}

// after the base case this function loops thorugh the entirety of the subtree of the given string y
// y is then modified and recursively calls this function untill all the words are visited
void restofword(struct tree *x, char *y, word * result)
{
	int i;
	struct tree *q = x;
	// if this is a word suggest it
	if(q->endofword)
	{
		if(q->score > result[0].score)
		{
			strcpy(result[1].w, result[0].w);
			result[1].score = result[0].score;
			strcpy(result[0].w, y);
			result[0].score = q->score;
		}
		else if(q->score > result[1].score)
		{
			strcpy(result[1].w, y);
			result[1].score = q->score;
		}
		// printf("%s\n", y);
	}
	// if not other children exists
	if(endnode(q))
	{
		return;
	}
	// go through the entire alphabet and 
	for(i = 0; i < 26; i++)
	{
		if(q->children[i])
		{
			y = add(y, 97 + i);

			restofword(q->children[i], y, result);

			sub(y);
		}
	}
}

// return -1 if the current word is the last thing in it's sub tree
// return 0 if nothing is found
// return 1 if everything went fine and there were 2 or more words in the sub tree
// modify the array word result to return the top 2 scores within the sub tree
// would like to have implemented the return value but didn't have time to do so
int printit(struct tree *x, const char *y, word *result)
{
	struct tree *p = x;

	int i;
	int length = strlen(y);
	for(i = 0; i < length; i++)
	{
		int ind = char_ind(y[i]);

		if(!p->children[ind])
		{
			return 0;
		}

		p = p->children[ind];
	}

	// if the prefix is a word
	int a;
	if(p->endofword)
	{
		a = 1;
	}
	else
	{
		a = 0;
	}

	// if the prefix doesn't have any children 
	int b = endnode(p);

	if(a && b)
	{
		//printf("%s\n", y);
		strcpy(result[0].w, y);
		return -1;
	}

	// if it isn't the last word

	if (!b)
	{
		char c[length];
		strcpy(c, y);
		restofword(p, c, result);
		return 1;
	}
	return 0;
}
